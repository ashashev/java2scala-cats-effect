# Сервис подписки на уведомления

Сделаем сервис, который будет отправлять уведомления всем пользователям, которые подписаны на определенные типы уведомлений.
При отправке уведомленй, необходимо записывать в базу факт начала уведомления и завершения.
В качестве мока уведомений будем использовать просто логирование с `Timer[F].sleep()`.

## Протокол

Регистрация пользователя
```
POST /user/register

{
    "username": ""
}

{
    "id": "UUID",
    "username": ""
}
```

Создание новой подписки
```
POST /subscription/create

{
    "description": "",
}

{
    "id": "UUID",
    "description": ""
}
```

Добавление нового пользователя в подписку
```
POST /subscription/$subscriptionId/subscribe

{
    "userId": "UUID",
}
```

Уведомление всех подписавшихся на подписку
```
POST /subscription/$subscriptionId/notify

{
    "text": ""
}
```

# Примерная структура хранения

```
CREATE TABLE users (
    id VARCHAR(64) PRIMARY KEY,
    username VARCHAR(256) NOT NULL
);

CREATE TABLE subscriptions (
    id VARCHAR(64) PRIMARY KEY,
    description TEXT
);

CREATE TABLE subscription_user (
    subscription_id VARCHAR(64) PRIMARY KEY,
    user_id VARCHAR(64) PRIMARY KEY
);

CREATE TABLE subscription_log (
    id VARCHAR(64) PRIMARY KEY,
    subscription_id VARCHAR(64),
    user_id VARCHAR(64),
    event_type VARCHAR(64),
    created_at TIMESTAMP 
);
```