package ru.tinkoff.java2scala.ex3

import java.util.UUID

import cats.effect._
import cats.implicits._ // В реальных проектах так не надо
import doobie.implicits._
import doobie.util.ExecutionContexts
import doobie.util.transactor.Transactor

final case class Todo(title: String, description: String)

// docker run -d --rm -p 5432:5432 -e POSTGRES_USER=demo -e POSTGRES_PASSWORD=demo -e POSTGRES_DB=demo postgres:latest
object Ex3App extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    val xa = Transactor.fromDriverManager[IO](
      "org.postgresql.Driver",
      "jdbc:postgresql://localhost/demo",
      "demo",
      "demo",
      Blocker.liftExecutionContext(ExecutionContexts.synchronous)
    )

    generateTodo(xa) >> listTodo(xa)
    .map { _ =>
      ExitCode.Success
    }
  }

  private def generateTodo(xa: Transactor[IO]): IO[Unit] = {
    val title = s"Todo#${UUID.randomUUID().toString}"
    val description = "Some description"
    sql"INSERT INTO todo (title, description) VALUES ($title, $description)".update.run.transact(xa).map(_ => ())
  }

  private def listTodo(xa: Transactor[IO]): IO[Unit] = {
    sql"SELECT title, description FROM todo"
      .query[Todo]
      .to[List]
      .transact(xa)
      .flatMap { result =>
        IO(println(s"Result: $result"))
      }
  }
}
