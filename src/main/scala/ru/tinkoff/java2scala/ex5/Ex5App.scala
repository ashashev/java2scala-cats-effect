package ru.tinkoff.java2scala.ex5

import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._

class Ex5App

object Ex5App extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    val logging = Logging(classOf[Ex5App])

    val log = Log[IO](classOf[Ex5App])

    val program = logging.info("Hello") >> log.info("World")
    program.map { _ =>
      ExitCode.Success
    }
  }
}
