package ru.tinkoff.java2scala.ex6

import cats.effect.{IO, Sync}
import cats.effect.concurrent.Ref
import cats.syntax.functor._

trait TodoStorage[F[_]] {
  def put(todo: Todo): F[Unit]
  def get(title: String): F[Option[Todo]]
}

class IOTodoStorage(ref: Ref[IO, Map[String, Todo]]) extends TodoStorage[IO] {
  override def put(todo: Todo): IO[Unit] = ref.modify(xs => (xs.updated(todo.title, todo), ()))
  override def get(title: String): IO[Option[Todo]] = ref.get.map(_.get(title))
}

class FTodoStorage[F[_]: Sync](ref: Ref[F, Map[String, Todo]]) extends TodoStorage[F] {
  override def put(todo: Todo): F[Unit] = ref.modify(xs => (xs.updated(todo.title, todo), ()))
  override def get(title: String): F[Option[Todo]] = ref.get.map(_.get(title))
}