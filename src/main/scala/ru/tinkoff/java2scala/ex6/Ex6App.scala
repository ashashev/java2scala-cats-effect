package ru.tinkoff.java2scala.ex6

import cats.effect.concurrent.Ref
import cats.effect.{ExitCode, IO, IOApp}

object Ex6App extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    for {
      ref <- Ref.of[IO, Map[String, Todo]](Map.empty)

      ioStorage = new IOTodoStorage(ref)
      _ <- ioStorage.put(Todo("IOStorage", "Value"))
      ioValue <- ioStorage.get("IOStorage")
      _ <- IO(println(s"IOStorage: $ioValue"))

      fStorage = new FTodoStorage[IO](ref)
      _ <- fStorage.put(Todo("FStorage", "Value"))
      fValue <- fStorage.get("FStorage")
      _ <- IO(println(s"FStorage: $fValue"))
    } yield ExitCode.Success
  }
}
