package ru.tinkoff.java2scala.ex6

final case class Todo(title: String, description: String)
