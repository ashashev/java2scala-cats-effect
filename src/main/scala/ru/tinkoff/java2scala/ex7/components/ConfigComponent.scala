package ru.tinkoff.java2scala.ex7.components

import cats.effect.{Resource, Sync}
import pureconfig._
import pureconfig.generic.auto._ // это нужно, хоть идея и говорит, что не нужно

object ConfigComponent {
  final case class ServerConfig(host: String, port: Int)
  final case class ApplicationConfig(server: ServerConfig)

  def apply[F[_]: Sync](): Resource[F, ApplicationConfig] = {
    Resource.liftF[F, ApplicationConfig](Sync[F].delay(ConfigSource.default.loadOrThrow[ApplicationConfig])) // ex2
  }
}
